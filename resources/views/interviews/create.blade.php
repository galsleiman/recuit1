@extends('layouts.app')

@section('title', 'Create interviews')

@section('content')
        <h1>Create interviews</h1>

        
       
        <form method = "post" action = "{{action('InterviewController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date_interviews">date interview</label>
            <input type = "date" class="form-control" name = "date_interviews">
        </div>     
        <div class="form-group">
            <label for = "summary">summary</label>
            <textarea row="7" class="form-control" name = "summary"></textarea>
        </div> 

        <div class="form-group row">
            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">candidate_id</label>
            <div class="col-md-6">
                <select class="form-control" name="candidate_id">                                                                         
                   @foreach ($candidates as $candidate)
        
                     <option value="{{ $candidate->id }}" selected="selected"> 
                         {{ $candidate->name }} 
                     </option>
                                               
                   @endforeach    
                 </select>
               
            </div>
        </div>

            <div class="form-group row">
                <label for="user_id" class="col-md-4 col-form-label text-md-right">user_id</label>
                <div class="col-md-6">
                    <select class="form-control" name="user_id">
                        <option value="{{ Auth::id() }}" selected="selected"> 
                            {{Auth::user()->name }} 
                        </option>                                                                    
                       @foreach ($users as $user)
                       
                         <option value="{{ $user->id }}" selected="selected"> 
                             {{ $user->name }} 
                         </option>
                                                   
                       @endforeach    
                     </select>
                   
                </div>
    
            
        <div>
            <input type = "submit" name = "submit" value = "Create intervies">
        </div>                               
        </form>    
@endsection
