@extends('layouts.app')

@section('title', 'Interview')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div>
 @if(sizeof($interviews)!=0)
<h1>List of Interviews</h1>
<table class = "table table-dark">
    <tr><th>id</th><th>date interviews</th><th>summary</th><th>Candidate  Interview</th><th>Interviewer</th>
    </tr>
    </tr>

  
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date_interviews}}</td>
            <td>{{$interview->summary}}</td>


            <td>
                
                  
                        @if (isset($interview->candidate_id))
                           {{$interview->candidates->name}}
                        @else
                            Define candidate
                        @endif
                                             
            </td>  
            <td>
                
                @if (isset($interview->status_id))
                   {{$interview->status->name}}
                @else
                    Define Interviewer
                @endif
                                     
    </td>  

        </tr>
        @endforeach
</table>
@else()
<div>
    <h1>Dont have interviews</h1>
</div>
@endif
</div>


