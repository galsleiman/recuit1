<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'date_interviews' => Carbon::now(),
            'summary' => Str::random(10)
            	
        ]);      
    }
  
}
