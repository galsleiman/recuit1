<?php

namespace App\Http\Controllers;
use App\Interview;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Role;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;




class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.index', compact('interviews','candidates','users'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function myInterview()
    {        

        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();
        

        return view('interviews.index', compact('interviews','users','user'));
    }


    public function create()

    {
        if(Gate::allows('add-interview')){
       
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create',compact('candidates','users'));
    } 
    else{
        
        abort(403,"You are not allowed to add the interview becuase you are not the admin or manger");
    }

     
        //
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

  
    public function store(Request $request)
    {
        $interview = new Interview();
    
        $int = $interview->create($request->all());
      
        $int->save();
        return redirect('interviews');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
